include_recipe 'chef-vault'

postgresql_server_install 'vexim' do
    action :install
    password chef_vault_item('w007_vexim', 'passwords')['postgres_users']['postgres']
end

postgresql_user 'vexim' do
    password chef_vault_item('w007_vexim', 'passwords')['postgres_users']['vexim']
end

postgresql_database 'vexim' do
    owner 'vexim'
end
