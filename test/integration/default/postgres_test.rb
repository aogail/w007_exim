describe service('postgresql') do
  it {should be_running}
  it {should be_installed}
end

describe postgres_hba_conf.where { type == 'local' && user == 'postgres' } do
  its('auth_method') { should cmp 'peer' }
end

describe postgres_hba_conf.where { type == 'local' && user == 'all' } do
  its('auth_method') { should cmp 'peer' }
end

describe postgres_hba_conf.where { type == 'host' } do
  its('address') { should eq %w(127.0.0.1/32 ::1/128) }
end

describe postgres_hba_conf.where { type == 'host' && address == '127.0.0.1/32' } do
  its('auth_method') { should cmp 'md5' }
end

describe postgres_hba_conf.where { type == 'host' && address == '::1/128' } do
  its('auth_method') { should cmp 'md5' }
end
