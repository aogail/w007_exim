describe package('exim4') do
  it {should be_installed}
end

describe service('exim4') do
  it {should be_running}
  it {should be_installed}
end