name 'w007_exim'
maintainer 'The Authors'
maintainer_email 'aogail@w007.org'
license 'MIT'
description 'Installs exim'
long_description ''
version '0.3.0'
chef_version '>= 12.10' if respond_to?(:chef_version)

source_url 'https://bitbucket.org/aogail/w007_exim/src'

depends 'postgresql'
depends 'chef-vault'
